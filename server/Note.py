import datetime

class Note(object):

    def __init__(self, note_dict):
        self.Name = note_dict["Name"]
        self.Email = note_dict["Email"]
        self.Opinion = note_dict["Opinion"]
        self.Date = datetime.date.today().strftime("%d/%m/%Y")

