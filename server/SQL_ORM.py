import sqlite3
from Note import Note


class Note_ORM(object):

    def __init__(self):
        self.conn = None  # will store the DB connection
        self.cursor = None  # will store the DB connection cursor
        self.current = None

    def Open_DB(self):
        """
        will open DB file and put value in:
        self.conn (need DB file name)
        and self.cursor
        """
        self.conn = sqlite3.connect('Notes_database.db')
        self.current = self.conn.cursor()

    def Close_DB(self):
        self.conn.close()

    def Commit(self):
        self.conn.commit()

    def Update_note(self, note: "Note"):
        self.Open_DB()

        sql = "INSERT INTO Notes(Name, Email, Opinion, Date)"
        sql += " VALUES('" + note.Name + "','" + note.Email + "','" + note.Opinion + "','" + note.Date + "')"
        self.current.execute(sql)
        self.Commit()

        self.Close_DB()
