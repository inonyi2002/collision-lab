import json
import hashlib

sep_char = "#"

ack_dict = {
    "CANVAS_DATA": "GOT_CANVAS",
    "NOTE": "GOTE_NOTE",
    "ADD_BALL": "BALL_ADDED",
    "REMOVE_BALL": "BALL_REMOVED",
    "PAUSE": "PAUSED",
    "PORT": "GOT_PORT",
    "CLOSE": "CLOSED"
}

error_dict = {
    1: "email address not found",
    2: "email is not valid"
}


def get_acknowledge(msg):
    return ack_dict[msg] + (sep_char * 3)


def get_error(err_number):
    return "ERR" + sep_char + error_dict[err_number] + (sep_char * 3)


def get_points_to_send(points_dict):
    to_send = "POINTS"
    to_send += sep_char

    keys_cnt = len(points_dict.keys())

    for i in range(keys_cnt):
        to_send += json.dumps(points_dict[i])
        if i != keys_cnt - 1:
            to_send += sep_char

    md5_hash = get_md5(to_send)
    to_send += md5_hash
    return to_send.encode()


def get_md5(s):
    return hashlib.md5(s.encode()).hexdigest()


