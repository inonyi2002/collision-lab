

class Point(object):

    def __init__(self, x, y):
        self.X = x
        self.Y = y

    def __str__(self):
        return "(" + str(self.X) + "," + str(self.Y) + ")"

