import Ball
import Point
import Board
import threading
import config
import math
from math import cos, sin, atan, pi
import numpy as np
lock_process = threading.Lock()

class Physical_computing(object):

    buffer_size = 20
    delta_time = 0.05
    g = 10  # 9.8


    def __init__(self, Balls_list, Cur_board):

        self.Balls_list = Balls_list
        self.Cur_board = Cur_board
        self.Point_buffer = {key: [] for key in range(len(self.Balls_list))}
        self.Engine_thread = None


    def Start(self, tid):

        self.Balls_list.sort(key=lambda b: b.Ball_number)
        self.Engine_thread = threading.Thread(target=self.__Ball_movement_calculation_by_time, args=(tid,))
        self.Engine_thread.start()



    def Stop(self):

        if self.Engine_thread is not None:
            self.Engine_thread.join()

        print("ball died !!!")


    def Get_data(self):

        lock_process.acquire()
        for value in self.Point_buffer.values():
            if len(value) < self.buffer_size:
                return None

        try:
            to_send_dict = {key: [] for key in range(len(self.Balls_list))}
            for key in self.Point_buffer.keys():
                val = self.Point_buffer[key]
                to_send_dict[key] = val[:self.buffer_size]
                self.Point_buffer[key] = val[self.buffer_size:]

            return to_send_dict
        finally:
            lock_process.release()


    def __Ball_movement_calculation_by_time(self, tid):

        while config.threads_play_dict[tid] is True:

            for ball in self.Balls_list:

                cur_ball_index = self.Balls_list.index(ball)
                collision_list = self.__Check_collisions(ball)

                if len(collision_list) != 0:
                    self.__Manage_collisions(ball, collision_list)
                    #self.Balls_list[cur_ball_index] = ball

                new_location = self.__Calac_next_step(ball)
                self.Balls_list[cur_ball_index] = ball
                self.Point_buffer[ball.Ball_number].append(new_location.__dict__)

                ball.Location = new_location


    def __Check_collisions(self, cur_ball):

        collision_list = []

        if cur_ball.Location.X - cur_ball.Radios <= self.Cur_board.Reference_location.X and cur_ball.Velocity_x < 0:
            collision_list.append(-1)

        if cur_ball.Location.Y - cur_ball.Radios <= self.Cur_board.Reference_location.Y and cur_ball.Velocity_y < 0:
            collision_list.append(-2)

        if cur_ball.Location.X + cur_ball.Radios >= self.Cur_board.Reference_location.X + self.Cur_board.Width and \
                cur_ball.Velocity_x > 0:
            collision_list.append(-3)

        if cur_ball.Location.Y + cur_ball.Radios >= self.Cur_board.Reference_location.Y + self.Cur_board.Height and \
                cur_ball.Velocity_y > 0:
            collision_list.append(-4)

        for ball in self.Balls_list:

            if ball == cur_ball:
                continue

            if self.__Distance(cur_ball.Location, ball.Location) <= cur_ball.Radios + ball.Radios - 0:
                collision_list.append(ball.Ball_number)

        return collision_list


    def __Manage_collisions(self, cur_ball, colliosion_list):

        if -1 in colliosion_list or -3 in colliosion_list:
            cur_ball.Velocity_x = -1 * cur_ball.Velocity_x

        if -2 in colliosion_list or -4 in colliosion_list:
            cur_ball.Velocity_y = -1 * cur_ball.Velocity_y

        walls_collision_cnt = len(list(filter(lambda x: x < 0, colliosion_list)))


        for ball_num in colliosion_list[walls_collision_cnt:]:

            ball_collide = self.Balls_list[ball_num]

            if abs(ball_collide.Location.Y - cur_ball.Location.Y) < 1:
                u1_x, u2_x = self.__Coliision_x(cur_ball, ball_collide)
                cur_ball.Velocity_x = u1_x
                cur_ball.Velocity_y = 0
                ball_collide.Velocity_x = u2_x
                ball_collide.Velocity_y = 0

            elif abs(ball_collide.Location.X - cur_ball.Location.X) < 1:
                u1_y, u2_y = self.__Collision_y(cur_ball, ball_collide)
                cur_ball.Velocity_y = u1_y
                cur_ball.Velocity_x = 0
                ball_collide.Velocity_y = u2_y
                ball_collide.Velocity_x = 0

            else:
                new_vx, new_vy = self.__Collsion_2d(cur_ball, ball_collide)

            self.Balls_list[ball_num] = ball_collide
            self.Balls_list[cur_ball.Ball_number] = cur_ball




    def __Coliision_x(self, cur_ball, other_ball):

        v1_x = cur_ball.Velocity_x
        v2_x = other_ball.Velocity_x
        m1 = cur_ball.Mass
        m2 = other_ball.Mass

        u1_x = ((m1 - m2) / (m1 + m2)) * v1_x
        tmp = ((2 * m2) / (m1 + m2)) * v2_x
        u1_x += tmp

        u2_x = ((2 * m1) / (m1 + m2)) * v1_x
        tmp = ((m2 - m1) / (m1 + m2)) * v2_x
        u2_x += tmp

        return u1_x, u2_x

    def __Collision_y(self, cur_ball, other_ball):

        v1_y = cur_ball.Velocity_y
        v2_y = other_ball.Velocity_y
        m1 = cur_ball.Mass
        m2 = other_ball.Mass

        u1_y = ((m1 - m2) / (m1 + m2)) * v1_y
        tmp = ((2 * m2) / (m1 + m2)) * v2_y
        u1_y += tmp

        u2_y = ((2 * m1) / (m1 + m2)) * u1_y
        tmp = ((m2 - m1) / (m1 + m2)) * u2_y
        u2_y += tmp

        return u1_y, u2_y

    def __Collision_2d(self, ball_1, ball_2):

        v1 = math.sqrt(pow(ball_1.Velocity_x, 2) + pow(ball_1.Velocity_y, 2))
        m1 = ball_1.Mass
        if abs(ball_1.Velocity_x) < 0.01:
            theta1 = 0
        elif abs(ball_1.Velocity_y) < 0.01:
            theta1 = pi / 2.0
        else:
            theta1 = atan(ball_1.Velocity_y / ball_1.Velocity_x) if ball_1.Velocity_x < 0 else \
                atan(ball_1.Velocity_y / ball_1.Velocity_x) + pi

        v2 = math.sqrt(pow(ball_2.Velocity_x, 2) + pow(ball_2.Velocity_y, 2))
        m2 = ball_2.Mass
        if abs(ball_2.Velocity_x) < 0.01:
            theta2 = 0
        elif abs(ball_2.Velocity_y) < 0.01:
            theta2 = pi / 2.0
        else:
            theta2 = atan(ball_2.Velocity_y / ball_2.Velocity_x) if ball_2.Velocity_x < 0 else \
                atan(ball_2.Velocity_y / ball_2.Velocity_x) + pi

        rot = atan((ball_1.Location.Y - ball_2.Location.Y) / (ball_1.Location.X - ball_2.Location.X))

        tmp1 = ((v1 * cos(theta1 - rot) * (m1 - m2)) + (2 * m2 * v2 * cos(theta2 - rot))) / (m1 + m2)
        u1_x = (tmp1 * cos(rot)) + (v1 * sin(theta1 - rot) * cos(rot + (pi/2)))
        u1_y = (tmp1 * sin(rot)) + (v1 * sin(theta1 - rot) * sin(rot + (pi/2)))

        tmp2 = ((v2 * cos(theta2 - rot) * (m2 - m1)) + (2 * m1 * v1 * cos(theta1 - rot))) / (m1 + m2)
        u2_x = (tmp2 * cos(rot)) + (v2 * sin(theta2 - rot) * cos(rot + (pi/2)))
        u2_y = (tmp2 * sin(rot)) + (v2 * sin(theta2 - rot) * sin(rot + (pi/2)))

    def __Get_collision_type(self, ball_1, ball_2):

        if abs(ball_1.Location.X - ball_2.Location.X) < 1:
            return 1
        if abs(ball_1.Location.Y - ball_2.Location.Y) < 1:
            return 2

        return 3



    def __Calac_next_step(self, ball):

        acc_x = ball.Kinetic_friction * self.g * math.copysign(1, ball.Velocity_x) * -1
        acc_y = ball.Kinetic_friction * self.g * math.copysign(1, ball.Velocity_y) * -1

        new_x, new_y = ball.Location.X, ball.Location.Y

        if ball.Velocity_x != 0:
            new_x = ball.Location.X + (ball.Velocity_x*self.delta_time) + (acc_x * pow(self.delta_time, 2)/2.0)
            new_vx = ball.Velocity_x + (acc_x * self.delta_time)

            if ball.Velocity_x > 0:
                new_x = max(new_x, ball.Location.X)
                ball.Velocity_x = max(0, new_vx)
            else:
                new_x = min(new_x, ball.Location.X)
                ball.Velocity_x = min(0, new_vx)

        if ball.Velocity_y != 0:
            new_y = ball.Location.Y + (ball.Velocity_y * self.delta_time) + (acc_y * pow(self.delta_time, 2)/2.0)
            new_vy = ball.Velocity_y + (acc_y * self.delta_time)

            if ball.Velocity_y > 0:
                new_y = max(new_y, ball.Location.Y)
                ball.Velocity_y = max(0, new_vy)

            else:
                new_y = min(new_y, ball.Location.Y)
                ball.Velocity_y = min(0, new_vy)


        return Point.Point(new_x, new_y)

    def __Manage_exceptional_movement(self):

        for i in range(len(self.Balls_list)):
            for j in range(i+1, len(self.Balls_list)):
                if self.__Distance(self.Balls_list[i].Location, self.Balls_list[j].Location) < \
                        self.Balls_list[i].Radios + self.Balls_list[j].Radios:

                    x = self.Balls_list[i].Location.X
                    y = self.Balls_list[i].Location.Y
                    m = ((self.Balls_list[j].Location.Y - y) / (self.Balls_list[j].Location.X - x))
                    A = pow(m, 2) + 1
                    B = (2 * x + 2 * y) * -1
                    C = pow(x, 2) + pow(y, 2) - pow(self.Balls_list[i].Radios + self.Balls_list[j].Radios + 1, 2)
                    sol1, sol2 = self.__Get_quadratic(A, B, C)

                    self.Balls_list[j].Location.X = sol1
                    self.Balls_list[j].Location.Y = sol1 * m

    def __Get_quadratic(self, a, b, c):

        d = (b ** 2) - (4 * a * c)
        # find two solutions
        sol1 = (-b - math.sqrt(d)) / (2 * a)
        sol2 = (-b + math.sqrt(d)) / (2 * a)
        return sol1, sol2


    def __Get_ball_by_number(self, ball_num):

        for ball in self.Balls_list:
            if ball.Ball_number == ball_num:
                return ball



    def Reset_buffer(self):
        self.Point_buffer = {key: [] for key in range(len(self.Balls_list))}

    # noinspection PyMethodMayBeStatic
    def __Distance(self, p1, p2):

        delta_x = p1.X - p2.X
        delta_y = p1.Y - p2.Y
        return math.sqrt(math.pow(delta_x, 2) + math.pow(delta_y, 2))







def main():

    #board = Board.Board({"Width":1425.0,"Height":628.0,"Reference_location":"0,0"})
    ball_1 = Ball.Ball({"location":"0,0","Ball_number":0,"velocity_x":37.0,"velocity_y":0.0,"mass":1.0,"radios":60.0,"kinetic_friction":0.0,"static_friction":0.0})
    ball_2 = Ball.Ball({"location":"30,0","Ball_number":1,"velocity_x":-37.0,"velocity_y":0.0,"mass":1.0,"radios":60.0,"kinetic_friction":0.0,"static_friction":0.0})

    pos1 = np.array([ball_1.Location.X, ball_1.Location.Y])
    pos2 = np.array([ball_2.Location.X, ball_2.Location.Y])

    delta = pos1 - pos2
    delta_length = np.linalg.norm(delta)

    mtd = delta * (((ball_1.Radios + ball_2.Radios) - delta_length) / delta_length)

    im1 = pow(ball_1.Mass, -1)
    im2 = pow(ball_2.Mass, -1)

    pos1 = pos1 + mtd * (im1 / (im1 + im2))
    pos2 = pos2 - mtd * (im2 / (im1 + im2))
    ball_1.Location = Point.Point(pos1[0], pos1[1])
    ball_2.Location = Point.Point(pos2[0], pos2[1])



if __name__ == '__main__':
    main()

