import yagmail
import re


class Email_sender(object):
    confirmation_msg = """this is confirmation message """
    confirmation_subject = "confirmation message"
    __mail = "inonyi2002@gmail.com"
    __password = "inon2002"

    def __init__(self):

        self.Smtp_connection = None

    def Connect(self):
        self.Smtp_connection = yagmail.SMTP(self.__mail, self.__password)

    def Is_valid_email(self, email):

        # for validating an Email
        regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        return bool(re.search(regex, email))


    def Send_confirmation_email(self, name, email, subject=None, message=None):

        self.Connect()

        if subject is None:
            subject = self.confirmation_subject

        if message is None:
            message = "Hello, " + str(name) + "\n"
            message += self.confirmation_msg

        try:
            self.Smtp_connection.send(email, subject, message)
            return True

        except Exception as ex:
            print(ex)
            return False
