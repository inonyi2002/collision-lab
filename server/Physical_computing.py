import Ball
import Point
import Board
import threading
import config
import math

from math import sin, cos, atan2
import numpy as np

lock_process = threading.Lock()


class Physical_computing(object):
    buffer_size = 100
    delta_time = 0.01
    g = 10  # 9.8
    impact_time = 0.001

    def __init__(self, Balls_list, Cur_board):

        self.Balls_list = Balls_list
        self.Cur_board = Cur_board
        self.Point_buffer = {key: [] for key in range(len(self.Balls_list))}
        self.Engine_thread = None

        print(str(self.Cur_board.Elasticity))

    def Start(self, tid):

        self.Balls_list.sort(key=lambda b: b.Ball_number)
        self.Engine_thread = threading.Thread(target=self.__Ball_movement_calculation_by_time, args=(tid,))
        self.Engine_thread.start()

    def Stop(self):

        if self.Engine_thread is not None:
            self.Engine_thread.join()
        self.Reset_buffer()
        print("ball died !!!")

    def Get_data(self):

        for value in self.Point_buffer.values():
            if len(value) < self.buffer_size:
                return None

        lock_process.acquire()
        try:
            to_send_dict = {key: [] for key in range(len(self.Balls_list))}
            for key in self.Point_buffer.keys():
                val = self.Point_buffer[key]
                to_send_dict[key] = val[:self.buffer_size]
                self.Point_buffer[key] = val[self.buffer_size:]

            return to_send_dict
        finally:
            lock_process.release()

    def __Ball_movement_calculation_by_time(self, tid):

        while config.threads_play_dict[tid] is True:

            for ball in self.Balls_list:

                ball.My_collisions = []
                cur_ball_index = self.Balls_list.index(ball)
                collision_list = self.__Check_collisions(ball)

                if len(collision_list) != 0:
                    self.__Manage_collisions(ball, collision_list)

                new_location = self.__Calac_next_step(ball)
                self.Balls_list[cur_ball_index] = ball
                self.Point_buffer[ball.Ball_number].append(new_location.__dict__)
                ball.Location = new_location

    def __Check_collisions(self, cur_ball):

        collision_list = []

        if cur_ball.Location.X - cur_ball.Radios <= self.Cur_board.Reference_location.X and cur_ball.Velocity_x < 0:
            collision_list.append(-1)

        if cur_ball.Location.Y - cur_ball.Radios <= self.Cur_board.Reference_location.Y and cur_ball.Velocity_y < 0:
            collision_list.append(-2)

        if cur_ball.Location.X + cur_ball.Radios >= self.Cur_board.Reference_location.X + self.Cur_board.Width and \
                cur_ball.Velocity_x > 0:
            collision_list.append(-3)

        if cur_ball.Location.Y + cur_ball.Radios >= self.Cur_board.Reference_location.Y + self.Cur_board.Height and \
                cur_ball.Velocity_y > 0:
            collision_list.append(-4)

        for ball in self.Balls_list:

            if ball == cur_ball:
                continue

            elif self.__Sq_Distance(cur_ball.Location, ball.Location) <= pow(cur_ball.Radios + ball.Radios, 2):
                if cur_ball.Ball_number not in self.Balls_list[ball.Ball_number].My_collisions and \
                        ball.Ball_number not in cur_ball.Collision_lock:
                    collision_list.append(ball.Ball_number)
                    cur_ball.Collision_lock.append(ball.Ball_number)

            elif ball.Ball_number in cur_ball.Collision_lock:
                cur_ball.Collision_lock.remove(ball.Ball_number)

        return collision_list

    def __Manage_collisions(self, cur_ball, colliosion_list):

        if -1 in colliosion_list or -3 in colliosion_list:
            cur_ball.Velocity_x = -1 * cur_ball.Velocity_x * self.Cur_board.Elasticity

        if -2 in colliosion_list or -4 in colliosion_list:
            cur_ball.Velocity_y = -1 * cur_ball.Velocity_y * self.Cur_board.Elasticity

        walls_collision_cnt = len(list(filter(lambda x: x < 0, colliosion_list)))

        for ball_num in colliosion_list[walls_collision_cnt:]:

            ball_collide = self.Balls_list[ball_num]
            cur_ball.My_collisions.append(ball_num)

            ball_1 = min(cur_ball, ball_collide, key=lambda b: b.Ball_number)
            ball_2 = max(cur_ball, ball_collide, key=lambda b: b.Ball_number)

            if ball_1.Is_static() and ball_1.Static_friction != 0:
                self.__Collision_static(ball_1, ball_2)

            elif ball_2.Is_static() and ball_2.Static_friction != 0:
                self.__Collision_static(ball_2, ball_1)

            else:
                self.__Collision_2d(ball_1, ball_2)

            pos1 = np.array([ball_1.Location.X, ball_1.Location.Y])
            pos2 = np.array([ball_2.Location.X, ball_2.Location.Y])

            delta = pos1 - pos2
            delta_length = np.linalg.norm(delta)

            mtd = delta * (((ball_1.Radios + ball_2.Radios) - delta_length) / delta_length)

            im1 = pow(ball_1.Mass, -1)
            im2 = pow(ball_2.Mass, -1)

            pos1 = pos1 + mtd * (im1 / (im1 + im2))
            pos2 = pos2 - mtd * (im2 / (im1 + im2))
            ball_1.Location = Point.Point(pos1[0], pos1[1])
            ball_2.Location = Point.Point(pos2[0], pos2[1])

            self.Balls_list[ball_1.Ball_number] = ball_1
            self.Balls_list[ball_2.Ball_number] = ball_2

    def __Collision_x(self, ball_1, ball_2):

        v1_x = ball_1.Velocity_x
        v2_x = ball_2.Velocity_x
        m1 = ball_1.Mass
        m2 = ball_2.Mass

        u1_x = ((m1 - m2) / (m1 + m2)) * v1_x
        tmp = ((2 * m2) / (m1 + m2)) * v2_x
        u1_x += tmp
        u1_x *= self.Cur_board.Elasticity

        u2_x = ((2 * m1) / (m1 + m2)) * v1_x
        tmp = ((m2 - m1) / (m1 + m2)) * v2_x
        u2_x += tmp
        u2_x *= self.Cur_board.Elasticity

        return u1_x, u2_x

    def __Collision_y(self, ball_1, ball_2):

        v1_y = ball_1.Velocity_y
        v2_y = ball_2.Velocity_y
        m1 = ball_1.Mass
        m2 = ball_2.Mass

        u1_y = ((m1 - m2) / (m1 + m2)) * v1_y
        tmp = ((2 * m2) / (m1 + m2)) * v2_y
        u1_y += tmp
        u1_y *= self.Cur_board.Elasticity

        u2_y = ((2 * m1) / (m1 + m2)) * v1_y
        tmp = ((m2 - m1) / (m1 + m2)) * v2_y
        u2_y += tmp
        u2_y *= self.Cur_board.Elasticity

        return u1_y, u2_y

    def __Collision_static(self, static_ball, other_ball):

        static_force = static_ball.Static_friction * static_ball.Mass * self.g
        other_v = abs(math.sqrt(pow(other_ball.Velocity_x, 2) + pow(other_ball.Velocity_y, 2)))
        avr_force = (2 * other_v * other_ball.Mass) / self.impact_time

        if avr_force > static_force:
            self.__Collision_2d(static_ball, other_ball)

        else:
            static_ball.Velocity_x = 0
            static_ball.Velocity_y = 0
            other_ball.Velocity_x = other_ball.Velocity_x * -1 * self.Cur_board.Elasticity
            other_ball.Velocity_y = other_ball.Velocity_y * -1 * self.Cur_board.Elasticity


    def __Collision_2d(self, ball_1, ball_2):

        v1 = math.sqrt(pow(ball_1.Velocity_x, 2) + pow(ball_1.Velocity_y, 2))
        m1 = ball_1.Mass
        direction_1 = atan2(ball_1.Velocity_y, ball_1.Velocity_x)

        v2 = math.sqrt(pow(ball_2.Velocity_x, 2) + pow(ball_2.Velocity_y, 2))
        m2 = ball_2.Mass
        direction_2 = atan2(ball_2.Velocity_y, ball_2.Velocity_x)

        rot = atan2(ball_1.Location.Y - ball_2.Location.Y, ball_1.Location.X - ball_2.Location.X)

        new_xspeed_1 = v1 * cos(direction_1 - rot)
        new_yspeed_1 = v1 * sin(direction_1 - rot)
        new_xspeed_2 = v2 * cos(direction_2 - rot)
        new_yspeed_2 = v2 * sin(direction_2 - rot)

        final_xspeed_1 = ((m1 - m2) * new_xspeed_1 + (m2 + m2) * new_xspeed_2) / (m1 + m2)
        final_xspeed_2 = ((m1 + m1) * new_xspeed_1 + (m2 - m1) * new_xspeed_2) / (m1 + m2)
        final_yspeed_1 = new_yspeed_1
        final_yspeed_2 = new_yspeed_2

        cos_angle = cos(rot)
        sin_angle = sin(rot)

        ball_1.Velocity_x = ((cos_angle * final_xspeed_1) - (sin_angle * final_yspeed_1)) * self.Cur_board.Elasticity
        ball_1.Velocity_y = ((sin_angle * final_xspeed_1) + (cos_angle * final_yspeed_1)) * self.Cur_board.Elasticity

        ball_2.Velocity_x = ((cos_angle * final_xspeed_2) - (sin_angle * final_yspeed_2)) * self.Cur_board.Elasticity
        ball_2.Velocity_y = ((sin_angle * final_xspeed_2) + (cos_angle * final_yspeed_2)) * self.Cur_board.Elasticity


    def __Calac_next_step(self, ball):

        acc_x = ball.Kinetic_friction * self.g * math.copysign(1, ball.Velocity_x) * -1
        acc_y = ball.Kinetic_friction * self.g * math.copysign(1, ball.Velocity_y) * -1

        new_x, new_y = ball.Location.X, ball.Location.Y

        if ball.Velocity_x != 0:
            new_x = ball.Location.X + (ball.Velocity_x * self.delta_time) + (acc_x * pow(self.delta_time, 2) / 2.0)
            new_vx = ball.Velocity_x + (acc_x * self.delta_time)

            if ball.Velocity_x > 0:
                new_x = max(new_x, ball.Location.X)
                if new_x + ball.Radios > self.Cur_board.Reference_location.X + self.Cur_board.Width:
                    new_x = self.Cur_board.Reference_location.X + self.Cur_board.Width - ball.Radios
                ball.Velocity_x = max(0, new_vx)

            else:
                new_x = min(new_x, ball.Location.X)
                if new_x - ball.Radios < self.Cur_board.Reference_location.X:
                    new_x = self.Cur_board.Reference_location.X + ball.Radios
                ball.Velocity_x = min(0, new_vx)

        if ball.Velocity_y != 0:
            new_y = ball.Location.Y + (ball.Velocity_y * self.delta_time) + (acc_y * pow(self.delta_time, 2) / 2.0)
            new_vy = ball.Velocity_y + (acc_y * self.delta_time)

            if ball.Velocity_y > 0:
                new_y = max(new_y, ball.Location.Y)
                if new_y + ball.Radios > self.Cur_board.Reference_location.Y + self.Cur_board.Height:
                    new_y = self.Cur_board.Reference_location.Y + self.Cur_board.Height - ball.Radios
                ball.Velocity_y = max(0, new_vy)

            else:
                new_y = min(new_y, ball.Location.Y)
                if new_y - ball.Radios < self.Cur_board.Reference_location.Y:
                    new_y = self.Cur_board.Reference_location.Y + ball.Radios
                ball.Velocity_y = min(0, new_vy)

        return Point.Point(round(new_x, 3), round(new_y, 3))

    def Reset_buffer(self):
        self.Point_buffer = {key: [] for key in range(len(self.Balls_list))}

    # noinspection PyMethodMayBeStatic
    def __Sq_Distance(self, p1, p2):
        return math.pow(p1.X - p2.X, 2) + math.pow(p1.Y - p2.Y, 2)


def main():
    board = Board.Board({"Width": 1425.0, "Height": 628.0, "Reference_location": "0,0"})
    b1 = Ball.Ball(
        {"location": "130,165", "Ball_number": 0, "velocity_x": 10.0, "velocity_y": 0.0, "mass": 1.0, "radios": 60.0,
         "kinetic_friction": 0.0, "static_friction": 0.0})
    b2 = Ball.Ball(
        {"location": "331,166", "Ball_number": 1, "velocity_x": 0.0, "velocity_y": 0.0, "mass": 100.0, "radios": 60.0,
         "kinetic_friction": 0.0, "static_friction": 0.0})

    ps = Physical_computing([b1, b2], board)
    ps.Start(1)


if __name__ == '__main__':
    main()
