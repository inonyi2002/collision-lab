import Point


class Board(object):

    def __init__(self, board_dict):

        x, y = board_dict["Reference_location"].split(",")

        self.Reference_location = Point.Point(float(x), float(y))
        self.Width = board_dict["Width"]
        self.Height = board_dict["Height"]
        self.Elasticity = board_dict["Elasticity"]




