import socket
import threading
import Protocol
import json
import Board
import Ball
import Physical_computing
import Note
import SQL_ORM
import EMAIL_MANAGER
import tcp_by_size
import config

lock_process = threading.Lock()


def tsafe_log_data(data):
    lock_process.acquire()
    with open("server_log.txt", 'a') as flog:
        flog.write(data + "\n")
    lock_process.release()


def add_client_to_dict():
    if config.threads_play_dict == {}:
        config.threads_play_dict[0] = False
        return 0
    else:
        max_key = max(config.threads_play_dict.keys()) + 1
        config.threads_play_dict[max_key] = False
        return max_key


def remove_client_from_dict(tid):
    lock_process.acquire()
    del config.threads_play_dict[tid]
    lock_process.release()


class ClientThread(threading.Thread):

    def __init__(self, tcp_sock, udp_sock, address, tid, ip):

        threading.Thread.__init__(self)
        self.tcp_sock = tcp_sock
        self.udp_sock = udp_sock
        self.address = address
        self.cur_pc = None
        self.tid = tid
        self.ip = ip

    def run(self) -> None:

        sep_char = Protocol.sep_char
        extension = sep_char * 3

        data = tcp_by_size.recv_by_size(self.tcp_sock)

        if data == "HELLO" + extension:
            tcp_by_size.send_with_size(self.tcp_sock, str("OLLHE" + extension))

        while True:

            try:
                data = tcp_by_size.recv_by_size(self.tcp_sock)
            except Exception as ex:
                print(ex)
                break

            data = data.rstrip(extension)
            # print(data)
            data_by_parts = data.split(sep_char)

            if data_by_parts[0] == "CANVAS_DATA":

                self.set_game_state(True)
                balls_list, board = self.get_canvas_data(data_by_parts[1:])
                tcp_by_size.send_with_size(self.tcp_sock, Protocol.get_acknowledge(data_by_parts[0]))

                data = tcp_by_size.recv_by_size(self.tcp_sock)

                if data.startswith("PORT"):
                    udp_port = int(data.split("#")[1])
                    tcp_by_size.send_with_size(self.tcp_sock, Protocol.get_acknowledge("PORT"))

                else:
                    tcp_by_size.send_with_size(self.tcp_sock, Protocol.get_acknowledge("CLOSE"))
                    self.set_game_state(False)
                    break

                addr = self.ip, udp_port
                print("got udp address: " + str(addr))

                udp_thread = threading.Thread(target=self.send_points, args=(balls_list, board, addr,))
                udp_thread.start()


            elif data_by_parts[0] == "PAUSE":
                self.set_game_state(False)
                udp_thread.join()
                tcp_by_size.send_with_size(self.tcp_sock, Protocol.get_acknowledge(data_by_parts[0]))
                print("udp thread died")

            elif data_by_parts[0] == "NOTE":
                ack = self.manage_note(data_by_parts[1])
                tcp_by_size.send_with_size(self.tcp_sock, ack)

            elif data_by_parts[0] == "CLOSE" or data_by_parts[0] == "":
                tcp_by_size.send_with_size(self.tcp_sock, Protocol.get_acknowledge("CLOSE"))
                self.set_game_state(False)
                break

        print("Thread died")
        if config.threads_play_dict[self.tid] is True:
            self.set_game_state(False)
            udp_thread.join()
        self.tcp_sock.close()

    def send_points(self, balls_list, board, addr):

        self.cur_pc = Physical_computing.Physical_computing(balls_list, board)
        self.cur_pc.Start(self.tid)
        while config.threads_play_dict[self.tid] is True:

            point_data = self.cur_pc.Get_data()
            if point_data is None:
                continue

            to_send = Protocol.get_points_to_send(point_data)
            try:
                self.udp_sock.sendto(to_send, addr)
                # print("messi !!!")

            except Exception as ex:
                print(ex)
                break

        print("here !!!")
        self.cur_pc.Stop()

    def set_game_state(self, state):
        lock_process.acquire()
        config.threads_play_dict[self.tid] = state
        lock_process.release()

    # noinspection PyMethodMayBeStatic
    def get_canvas_data(self, canvas_data_json):

        board_dict = json.loads(canvas_data_json[0])
        board = Board.Board(board_dict)

        balls_list = []

        for ball_string in canvas_data_json[1:]:
            ball_dict = json.loads(ball_string)
            balls_list.append(Ball.Ball(ball_dict))

        return balls_list, board

    # noinspection PyMethodMayBeStatic
    def manage_note(self, json_note):

        note_obj = Note.Note(json.loads(json_note))

        ack = Protocol.get_acknowledge("NOTE")

        if note_obj.Email != "None":

            email_handler = EMAIL_MANAGER.Email_sender()
            is_valid = email_handler.Is_valid_email(note_obj.Email)
            if is_valid is True:
                is_send = email_handler.Send_confirmation_email(note_obj.Name, note_obj.Email)
                if is_send is False:
                    note_obj.Email = str(None)
                    ack = Protocol.get_error(1)
            else:
                note_obj.Email = str(None)
                ack = Protocol.get_error(2)

        db_handler = SQL_ORM.Note_ORM()
        db_handler.Update_note(note_obj)
        return ack


def main():
    config.initialize()

    host_ip = "0.0.0.0"

    tcp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_port = 2002

    udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_port = 11000

    tcp_sock.bind((host_ip, tcp_port))
    udp_sock.bind((host_ip, udp_port))

    tcp_sock.listen(12)

    threads = []

    while True:

        try:
            new_tcp_sock, addr = tcp_sock.accept()
            cur_tid = add_client_to_dict()
            print(config.threads_play_dict)
            print("new client, ip = " + str(addr[0]) + " port = " + str(addr[1]) + "\n")
            new_theard = ClientThread(new_tcp_sock, udp_sock, addr, cur_tid, str(addr[0]))
            new_theard.start()
            threads.append(new_theard)

        except socket.timeout as ex:
            print("exception, " + str(ex))

    tcp_sock.close()
    udp_sock.close()

    for t in threads:
        t.join()

    print("done for today !")


if __name__ == '__main__':
    main()
