import Point


class Ball(object):

    def __init__(self, ball_dict):

        x, y = ball_dict["location"].split(",")

        self.Location = Point.Point(float(x), float(y))
        self.Velocity_x = ball_dict["velocity_x"]
        self.Velocity_y = ball_dict["velocity_y"]
        self.Mass = ball_dict["mass"]
        self.Radios = ball_dict["radios"]
        self.Kinetic_friction = ball_dict["kinetic_friction"]
        self.Static_friction = ball_dict["static_friction"]
        self.Ball_number = ball_dict["Ball_number"]

        self.My_collisions = []
        self.Collision_lock = []

    def Is_static(self):
        return (self.Velocity_x == 0) and (self.Velocity_y == 0)



