﻿using client.all_classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace client
{
    /// <summary>
    /// Interaction logic for Write_us_page.xaml
    /// </summary>
    public partial class Write_us_page : Page
    {
       
        public Write_us_page()
        {
            InitializeComponent();
            double width = ((Panel)Application.Current.MainWindow.Content).ActualWidth;
            double height = ((Panel)Application.Current.MainWindow.Content).ActualHeight;

            Note_panel.Height = height;
            Note_panel.Width = width / 3;
        }

        private void Back_click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void Clear_click(object sender, RoutedEventArgs e)
        {
            Name_textbox.Text = "";
            Email_textbox.Text = "";
            Opinion_textbox.Text = "";
        }


        private void Send_click(object sender, RoutedEventArgs e)
        {
            string name = Name_textbox.Text.Trim();
            string email = Email_textbox.Text.Trim();
            string opinion = Opinion_textbox.Text.Trim();

            string msg; 

            if (name == "" || opinion == "")
            {
                msg = "Name or opinion missing";
                MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (email == "")
            {
                msg = "Are you sure you want to note us without your mail ?";
                MessageBoxResult result = MessageBox.Show(msg, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if (result == MessageBoxResult.No)
                    return;
            }


            Note cur_note = new Note(name, email, opinion);

            string to_send = Protocol.Get_note(cur_note);
            Tcp_communication.Send(to_send);
            string ack = Tcp_communication.Receive();

            if (ack.Contains("valid"))
            {
                msg = "The email address is not valid !";
                MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                Email_textbox.Text = "";
                return;
            }

            Name_textbox.Text = "";
            Email_textbox.Text = "";
            Opinion_textbox.Text = "";
        }


        //email address validate
        //private bool Is_email_valid(string email)
        //{
        //    string theEmailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
        //                           + "@"
        //                           + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";

        //    return Regex.IsMatch(email, theEmailPattern); 
        //}

    }
}
