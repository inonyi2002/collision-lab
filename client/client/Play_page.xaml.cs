﻿using client.all_classes;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace client
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Cursor_point
    {
        public int X;
        public int Y;

        public Cursor_point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }


    /// <summary>
    /// Interaction logic for Play_page.xaml
    /// </summary>
    public partial class Play_page : Page, INotifyPropertyChanged
    {

        private readonly SolidColorBrush[] Ball_color_arr = { Brushes.Red, Brushes.Blue, Brushes.DeepPink, Brushes.Yellow, Brushes.Chocolate, Brushes.Green }; 

        Queue<Point>[] Points_stream;
        Board Canvas_board;
        ObservableCollection<Ball> balls_list;
        Ball[] Copy_balls_array;
        Thread Udp_thread;
        Recorder Record_handler;
        System.Timers.Timer Movement_timer;
        System.Timers.Timer _Timer;
        bool Is_play;
        bool Is_kinetic_energy;
        bool Is_record;
        double Time_past;
        int Page_with;
        int Page_Height;



        private const double timer_constant = 10;
        private object Lock_object = new object();

        public Play_page()
        {
            InitializeComponent();

            this.balls_list = new ObservableCollection<Ball>();
            this.Copy_balls_array = null;
            this.Canvas_board = new Board(GameCanvas.ActualWidth, GameCanvas.ActualHeight, 1, GameCanvas.TranslatePoint(new Point(0, 0), this));
            this.Is_play = false;
            this.Is_kinetic_energy = false;
            this.Is_record = false;
            DataContext = this;
            this.Time_past = 0;
            this.Page_with = 0;
            this.Page_Height = 0;
            this.Udp_thread = null;
            this.Record_handler = null; 


            this.Movement_timer = new System.Timers.Timer(timer_constant);
            this.Movement_timer.Elapsed += Movement_timer_Elapsed;
            this.Movement_timer.AutoReset = true;

            this._Timer = new System.Timers.Timer(timer_constant);
            this._Timer.Elapsed += Timer_handler;
            this._Timer.AutoReset = true;

            Application.Current.Exit += new ExitEventHandler(Page_exit);
        }



        //////////////////////////////////Extern functions/////////////////////////////////////////////////////////////////////////
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out Cursor_point lpPoint); //give the mouse position in win32 pixel unit 

        [DllImport("User32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //for binding
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        public ObservableCollection<Ball> Balls_list
        {
            get { return balls_list; }
            set
            {
                balls_list = value;
                OnPropertyChanged("Balls_list");
            }
        }


        private void Add_click(object sender, RoutedEventArgs e)
        {
            if (this.balls_list.Count == 6 || this.Is_play) return;

            Add_ball_to_canvas();
        }


        /// <summary>
        /// Remove ball from canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove_click(object sender, RoutedEventArgs e)
        {
            if (this.balls_list.Count == 0 || this.Is_play) return;

            Ball remove_ball = this.balls_list[this.balls_list.Count - 1];
            this.balls_list.Remove(remove_ball);
            GameCanvas.Children.Remove(remove_ball.Graphic_ball);
        }


        /// <summary>
        /// Add ball to canvas
        /// </summary>
        private void Add_ball_to_canvas()
        {
            Ball cur_ball = new Ball();

            Ellipse cur_gr_ball = new Ellipse();
            cur_gr_ball.Height = cur_ball.Radios * 2;
            cur_gr_ball.Width = cur_ball.Radios * 2;
            cur_gr_ball.Fill = Ball_color_arr[this.balls_list.Count];
            cur_gr_ball.Stroke = Brushes.Black;
            cur_gr_ball.AllowDrop = true;

            cur_gr_ball.MouseDown += Ball_mouse_down;
            cur_ball.Graphic_ball = cur_gr_ball;
            Point ball_position = Get_available_position(cur_gr_ball);

            if (GameCanvas.Children.Count == 0 )
            {
                ball_position = new Point(ball_position.X + GameCanvas.ActualWidth / 2, ball_position.Y + GameCanvas.ActualHeight / 2);
            }

            GameCanvas.Children.Add(cur_gr_ball);
            Canvas.SetLeft(cur_gr_ball, ball_position.X);
            Canvas.SetTop(cur_gr_ball, ball_position.Y);

            Point ball_center = new Point(ball_position.X + cur_gr_ball.Width / 2d, ball_position.Y + cur_gr_ball.Height / 2d);
            cur_ball.Location = ball_center;
            cur_ball.Ball_number = this.balls_list.Count;
            cur_ball.Observable_ball_number = this.balls_list.Count + 1;
            this.Balls_list.Add(cur_ball);

            int row_index = this.balls_list.Count - 1; 
            DataGridRow row = (DataGridRow)MyDt.ItemContainerGenerator.ContainerFromIndex(row_index);
            if (row == null)
            {
                MyDt.UpdateLayout();
                MyDt.ScrollIntoView(MyDt.Items[row_index]);
                row = (DataGridRow)MyDt.ItemContainerGenerator.ContainerFromIndex(row_index);
            }
            row.Background = Ball_color_arr[row_index];
        }


        /// <summary>
        /// find and return avaiable posion on canvas
        /// </summary>
        /// <param name="ball"></param>
        /// <returns> point (x, y) </returns>
        private Point Get_available_position(Ellipse ball)
        {
            double radios = ball.Width / 2d;
            Point tmp_ball_center = new Point(0 + radios, 0 + radios);

            int rounds = (int)GameCanvas.ActualHeight * (int)GameCanvas.ActualWidth;
            bool is_break = false;

            for (int i = 0; i < rounds; i++)
            {
                foreach (var b in this.balls_list)
                {
                    double other_location_x = Canvas.GetLeft(b.Graphic_ball) + (b.Graphic_ball.Width / 2d);
                    double other_location_y = Canvas.GetTop(b.Graphic_ball) + (b.Graphic_ball.Height / 2d);
                    double distance = Movement_calculation.Distance(tmp_ball_center, new Point(other_location_x, other_location_y));

                    if (distance < b.Radios + radios)
                    {
                        is_break = true;
                        break;
                    }
                }

                if (!is_break)
                    break;

                is_break = false;
                double new_x = (tmp_ball_center.X + radios + 1 > GameCanvas.ActualWidth) ? radios : tmp_ball_center.X + 1;
                double new_y = tmp_ball_center.Y;
                if (new_x == radios)
                {
                    new_y = (tmp_ball_center.Y + radios + 1 > GameCanvas.ActualHeight) ? radios : tmp_ball_center.Y + 1;
                }
                tmp_ball_center = new Point(new_x, new_y);
            }

            return new Point(tmp_ball_center.X - radios, tmp_ball_center.Y - radios);

        }


        /// <summary>
        /// start drag and drop when mouse is presses on ellipse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ball_mouse_down(object sender, MouseEventArgs e)
        {
            if (this.Is_play) return;

            Ellipse p = sender as Ellipse;
            DataObject data_obj = new DataObject(p);
            DragDrop.DoDragDrop(p, data_obj, DragDropEffects.All);

        }


        /// <summary>
        /// finish drag and drop check drop location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameCanvas_Drag(object sender, DragEventArgs e)
        {

            Cursor_point cp;
            GetCursorPos(out cp);
            Point wpfPoint = ConvertPixelsToUnits(cp.X, cp.Y);
            Ellipse ball = (Ellipse)e.Data.GetData(typeof(Ellipse));
            Point point_fixed_to_board = Set_drop_point(ball, new Point(wpfPoint.X - 15, wpfPoint.Y - 23));

            Canvas.SetLeft(ball, point_fixed_to_board.X);
            Canvas.SetTop(ball, point_fixed_to_board.Y);


            Set_center_location(ball, point_fixed_to_board);
        }



        /// <summary>
        /// check if drop point is valid if not fix it
        /// </summary>
        /// <param name="ball"> the ball </param>
        /// <param name="p"> drop point</param>
        /// <returns></returns>
        private Point Set_drop_point(Ellipse ball, Point p)
        {

            Point canvas_location = GameCanvas.TranslatePoint(new Point(0, 0), this);

            double the_x = p.X;
            double the_y = p.Y;
            double delta_x;
            double delta_y;

            if (the_x + ball.Width > canvas_location.X + GameCanvas.ActualWidth)
            {
                delta_x = (the_x + ball.Width) - GameCanvas.ActualWidth;
                the_x -= delta_x;
            }

            else if (the_x < canvas_location.X)
            {
                delta_x = canvas_location.X - the_x;
                the_x += delta_x;
            }


            if (the_y + ball.Height > canvas_location.Y + GameCanvas.ActualHeight)
            {
                delta_y = (the_y + ball.Height) - GameCanvas.ActualHeight;
                the_y -= delta_y;
            }

            else if (the_y < canvas_location.Y)
            {
                delta_y = canvas_location.Y - the_y;
            }

            return new Point(the_x, the_y);
        }


        private Point ConvertPixelsToUnits(int x, int y)
        {
            // get the system DPI
            IntPtr dDC = GetDC(IntPtr.Zero); // Get desktop DC
            int dpi = GetDeviceCaps(dDC, 88);
            ReleaseDC(IntPtr.Zero, dDC);

            // WPF's physical unit size is calculated by taking the 
            // "Device-Independant Unit Size" (always 1/96)
            // and scaling it by the system DPI
            double physicalUnitSize = (1d / 96d) * (double)dpi;
            Point wpfUnits = new Point(physicalUnitSize * (double)x,
                physicalUnitSize * (double)y);

            return wpfUnits;

        }

        /// <summary>
        /// for debug mostly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Play_Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double proportion_x = GameCanvas.ActualWidth / Canvas_board.Width;
            double proportion_y = GameCanvas.ActualHeight / Canvas_board.Height;

            this.Canvas_board.Width = GameCanvas.ActualWidth;
            this.Canvas_board.Height = GameCanvas.ActualHeight;

            foreach (Ball b in this.balls_list)
            {
                double last_ball_x = b.Location.X - b.Radios;
                double last_ball_y = b.Location.Y - b.Radios;

                Point new_location = Set_drop_point(b.Graphic_ball, new Point(last_ball_x * proportion_x, last_ball_y * proportion_y));

                Canvas.SetLeft(b.Graphic_ball, new_location.X);
                Canvas.SetTop(b.Graphic_ball, new_location.Y);
                Set_center_location(b.Graphic_ball, new_location);
            }

        }

        /// <summary>
        /// set center location of all balls on canvas
        /// </summary>
        /// <param name="ellipse"></param>
        /// <param name="location"></param>
        private void Set_center_location(Ellipse ellipse, Point location)
        {
            foreach (Ball b in this.balls_list)
            {
                if (ellipse == b.Graphic_ball)
                {
                    Point new_center = new Point(location.X + b.Radios, location.Y + b.Radios);
                    b.Location = new_center;
                    break;
                }
            }
        }

       
        /// <summary>
        /// try to kill the udp listener if user close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_exit(object sender, ExitEventArgs e)
        {
            Udp_thread?.Abort();
        }


        /// <summary>
        /// reset canvas give all balls the initiate data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Restart_click(object sender, RoutedEventArgs e)
        {

            if (this.Is_play || this.balls_list.Count == 0) return;

            for (int i = 0; i < this.Copy_balls_array.Length; i++)
            {
                this.balls_list[i].Kinetic_friction = this.Copy_balls_array[i].Kinetic_friction;
                this.balls_list[i].Location = this.Copy_balls_array[i].Location;
                this.balls_list[i].Mass = this.Copy_balls_array[i].Mass;
                this.balls_list[i].Radios = this.Copy_balls_array[i].Radios;
                this.balls_list[i].Static_friction = this.Copy_balls_array[i].Static_friction;
                this.balls_list[i].Velocity_x = this.Copy_balls_array[i].Velocity_x;
                this.balls_list[i].Velocity_y = this.Copy_balls_array[i].Velocity_y;
                this.balls_list[i].Graphic_ball.Width = this.balls_list[i].Radios * 2;
                this.balls_list[i].Graphic_ball.Height = this.balls_list[i].Radios * 2;

                Canvas.SetLeft(this.balls_list[i].Graphic_ball, this.balls_list[i].Location.X - this.balls_list[i].Radios);
                Canvas.SetTop(this.balls_list[i].Graphic_ball, this.balls_list[i].Location.Y - this.balls_list[i].Radios);
            }
            this.Timer_label.Content = "Time = 0.00";
            this.Time_past = 0; 
        }


        /// <summary>
        /// stop simulation and close udp listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Stop_click(object sender, RoutedEventArgs e)
        {
            if (!this.Is_play) return;

            this.Is_play = false;
            MyDt.IsReadOnly = false;
            this.Elasticity_slider.IsEnabled = true;
            this.Movement_timer.Stop();
            this._Timer.Stop();
            string to_send = Protocol.Get_pause();
            Tcp_communication.Send(to_send);
            string ack = Tcp_communication.Receive();
            this.Udp_thread?.Join();
            //this.Udp_thread.Abort(); 

        }


        /// <summary>
        /// the target function of the udp thread
        /// initiate udp communication and receive points 
        /// </summary>
        private void Udp_listener()
        {
            Udp_communication.Connect();

            string addr_to_send = "PORT#" + Udp_communication.Get_port();
            Tcp_communication.Send(addr_to_send);
            string ack_data = Tcp_communication.Receive();

            while (this.Is_play)
            {
                string data = Udp_communication.Receive();

                if (!data.StartsWith("POINTS") || data.Length < 32)
                    continue;

                string point_data = data.Substring(0, data.Length - 32);
                string hash = data.Substring(data.Length - 32, 32);

                if (!Md5_hash.VerifyMd5Hash(System.Security.Cryptography.MD5.Create(), point_data, hash))
                    continue;


                string[] points_array = point_data.Split('#');

                for (int i = 1; i < points_array.Length; i++)
                {
                    Point_json[] cur_arr = JsonConvert.DeserializeObject<Point_json[]>(points_array[i]);

                    lock (this.Lock_object)
                    {
                        for (int j = 0; j < cur_arr.Length; j++)
                        {
                            this.Points_stream[i - 1].Enqueue(cur_arr[j].Convert_to_point());
                            //Dbug.write_to_file(cur_arr[j].ToString());
                        }
                    }
                }


            }
            Udp_communication.End_connection();
        }


        /// <summary>
        /// send canvas data to server and start the udp listener and movement timer and the time label timer 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Play_click(object sender, RoutedEventArgs e)
        {
            if (this.balls_list.Count == 0 || this.Is_play) return;

            this.Is_play = true;
            MyDt.IsReadOnly = true;
            this.Elasticity_slider.IsEnabled = false; 

            this.Copy_balls_array = new Ball[this.balls_list.Count];
            this.Points_stream = new Queue<Point>[this.balls_list.Count];


            for (int i = 0; i < this.Points_stream.Length; i++)
            {
                this.Points_stream[i] = new Queue<Point>();
                this.Copy_balls_array[i] = this.balls_list[i].Clone() as Ball;
            }

            this.Canvas_board.Elasticity = this.Elasticity_slider.Value / 100d;
            string to_send = Protocol.Get_all_canvas_data(this.balls_list, this.Canvas_board);
            Tcp_communication.Send(to_send);
            string tcp_data = Tcp_communication.Receive();

            Udp_thread = new Thread(() => Udp_listener());
            Udp_thread.Start();

            Movement_timer.Start();
            _Timer.Start(); 
        }


        /// <summary>
        /// this function calls every 10 milisec and it moves the balls according to points_stream variable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Movement_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                for (int i = 0; i < this.balls_list.Count; i++)
                {
                    lock (this.Lock_object)
                    {
                        if (this.Points_stream[i].Count != 0)
                        {
                            Point next_point = this.Points_stream[i].Dequeue();
                            Canvas.SetLeft(this.balls_list[i].Graphic_ball, next_point.X - this.balls_list[i].Radios);
                            Canvas.SetTop(this.balls_list[i].Graphic_ball, next_point.Y - this.balls_list[i].Radios);

                            Point last_point = this.balls_list[i].Location;

                            this.balls_list[i].Location = next_point;

                            this.balls_list[i].Velocity_x = Movement_calculation.Calc_velocity_x(last_point, next_point);
                            this.balls_list[i].Velocity_y = Movement_calculation.Calc_velocity_y(last_point, next_point);


                            if (this.Is_kinetic_energy)
                            {
                                double cur_ke = Movement_calculation.Calc_total_energy(this.balls_list);
                                KE_label.Content = "Kinetic energy = " + cur_ke.ToString();
                            }

                        }
                    }
                }
            });

        }


        /// <summary>
        /// check if the physical data is valid if not change it randomly 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyDt_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Random rnd = new Random();
            string msg;
            if (e.EditAction == DataGridEditAction.Commit)
            {
                DataGridBoundColumn columm = e.Column as DataGridBoundColumn;

                if (columm != null)
                {
                    int row_index = e.Row.GetIndex();
                    if (columm.Header.ToString() == "Radios")
                    {
                        TextBox new_radios = e.EditingElement as TextBox;
                        double radios = double.Parse(new_radios.Text);

                        if (radios <= 0)
                        {
                            msg = "radios mast be greater than zero !";
                            MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                            radios = rnd.Next(20, 60);
                            this.balls_list[row_index].Radios = radios;
                        }

                        this.balls_list[row_index].Graphic_ball.Width = radios * 2;
                        this.balls_list[row_index].Graphic_ball.Height = radios * 2;
                    }

                    else if (columm.Header.ToString() == "Mass")
                    {
                        TextBox new_mass = e.EditingElement as TextBox;
                        double mass = double.Parse(new_mass.Text);

                        if (mass <= 0)
                        {
                            msg = "mass mast be greater than zero !";
                            MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                            mass = rnd.Next(1, 4);
                            this.balls_list[row_index].Mass = mass;
                        }
                        else if (mass > 50)
                        {
                            msg = "mass too big (must be below 50)";
                            MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                            mass = rnd.Next(1, 4);
                            this.balls_list[row_index].Mass = mass;
                        }

                    }

                    else if (columm.Header.ToString() == "Kinetic friction")
                    {
                        TextBox new_friction = e.EditingElement as TextBox;
                        double friction = double.Parse(new_friction.Text);

                        if (friction < 0 || friction > 1)
                        {
                            msg = "friction mast be greater than zero and smaller than one !";
                            MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                            friction = Math.Round(rnd.NextDouble(), 3);
                            this.balls_list[row_index].Kinetic_friction = friction;
                        }
                    }

                    else if (columm.Header.ToString() == "Static friction")
                    {
                        TextBox new_friction = e.EditingElement as TextBox;
                        double friction = double.Parse(new_friction.Text);

                        if (friction < 0 || friction > 1)
                        {
                            msg = "friction mast be greater than zero and smaller than one !";
                            MessageBox.Show(msg, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                            friction = Math.Round(rnd.NextDouble(), 3);
                            this.balls_list[row_index].Static_friction = friction;
                        }
                    }

                }

            }
        }


        /// <summary>
        /// in case user want to watch the total kinetic energy on the canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (this.balls_list.Count == 0) return;

            this.Is_kinetic_energy = true;

        }


        /// <summary>
        /// in case user dont want to watch the total kinetic energy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            this.Is_kinetic_energy = false;
            KE_label.Content = string.Empty;
            
        }


        /// <summary>
        /// go back to open page
        /// if simulation running stop it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Back_click(object sender, RoutedEventArgs e)
        {
            if (this.Is_play)
            {
                this.Is_play = false;
                this.Movement_timer.Stop();
                string to_send = Protocol.Get_pause();
                Tcp_communication.Send(to_send);
                this.Udp_thread?.Join();
            }
            NavigationService.GoBack();
        }


        /// <summary>
        /// start record or stop record (simulation)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Record_click(object sender, RoutedEventArgs e)
        {
            if (this.Is_record)
            {

                Task.Factory.StartNew(() =>
                { 
                    Record_handler.Dispose();
                });
                this.Is_record = false;
                Record_icon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Record;

            }

            else
            {

                Record_icon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Stop; 

                string file_name = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + 
                    @"\records\" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".avi"; // desktop path + current time
                int fps = 25;
                this.Record_handler = new Recorder(new RecorderParams(file_name, fps, SharpAvi.KnownFourCCs.Codecs.MotionJpeg, 70));
                this.Is_record = true;
                this.Record_handler.Start();
            }

        }


        /// <summary>
        /// set new Elasticity modulus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Elasticity_slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            double new_e = this.Elasticity_slider.Value / 100d;
            if (new_e == 1.0)
            {
                int c = 0;
            }
            this.Canvas_board.Elasticity = new_e; 

            
        }


        /// <summary>
        /// open the user guide pdf 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Help_click(object sender, RoutedEventArgs e)
        {
            Process.Start(@"Resources\manual.pdf");
        }



        /// <summary>
        /// this function calld every 10 milisec 
        /// adds tine to timer 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        int Reset_cnt = 0;
        private void Timer_handler(object sender, ElapsedEventArgs e)
        {
            this.Time_past += (timer_constant / 1000d);
            Dispatcher.Invoke(() =>
            {
                Timer_label.Content = "Time = " + Math.Round(this.Time_past, 3).ToString();
                if (Reset_cnt == 10_000) //Stop and then play every 100 seconds 
                {
                    this.Stop_button.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    this.Play_button.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    Reset_cnt = 0;
                }
            });

            Reset_cnt++;
        }


    }
}
