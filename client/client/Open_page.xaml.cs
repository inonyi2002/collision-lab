﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace client
{
    /// <summary>
    /// Interaction logic for Open_page.xaml
    /// </summary>
    public partial class Open_page : Page
    {
        public Open_page()
        {
            InitializeComponent();

            double width = ((Panel)Application.Current.MainWindow.Content).ActualWidth;

            Head_label.FontSize = (int)(width / 18);
            Play_button.FontSize = (int)(width / 33);
            Write_button.FontSize = (int)(width / 33);
            Close_button.FontSize = (int)(width / 33); 
        }

        private void Play_click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Play_page());
        }

        private void Write_us_click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Write_us_page());
        }

        private void Close_click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

    }
}
