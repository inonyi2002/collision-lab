﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace client.all_classes
{
    static class Udp_communication
    {
        static private readonly int Port = 11000;
        static private readonly string Ip = "127.0.0.1";

        static private IPEndPoint IpLocalEndPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
        static public UdpClient Udp_client = null;

        public static bool Connect()
        {
            try
            {

                Udp_client = new UdpClient();
                Udp_client.Connect(IpLocalEndPoint);
                Udp_client.Client.ReceiveTimeout = 2500;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eror", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        public static void Send(string data)
        {
            byte[] to_send = Encoding.ASCII.GetBytes(data);
            Udp_client.Send(to_send, to_send.Length);
        }

        public static string Receive()
        {
            try
            {
                byte[] data = Udp_client.Receive(ref IpLocalEndPoint);
                return Encoding.ASCII.GetString(data);
            }
            catch (Exception ex)
            {

                return "";
            }
        }

        public static string Get_port()
        {
            int conn_port = ((IPEndPoint)Udp_client.Client.LocalEndPoint).Port;
            return conn_port.ToString();
        }

        public static void End_connection()
        {
            Udp_client.Close();
            Udp_client = null;
        }


    }
}
