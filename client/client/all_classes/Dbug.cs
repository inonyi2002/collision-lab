﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client.all_classes
{
    static class Dbug
    {
        public const string file_path = @"F:\final_project\client\debug.txt";


        public static void write_to_file(string str)
        {
            if (!is_file_exist(file_path))
                create_file(file_path); 

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(file_path, true))
                file.Write(str + "\n");
        }

        private static bool is_file_exist(string path)
        {
            return System.IO.File.Exists(path);
        }

        private static void create_file(string path)
        {
            var myFile = File.Create(path);
            myFile.Close();
        }

        public static void delete_file()
        {
            if (is_file_exist(file_path))
            {
                File.Delete(file_path);
            }
        }
    }
}
