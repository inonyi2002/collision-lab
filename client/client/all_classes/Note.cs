﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client.all_classes
{
    class Note: Isupport_json
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Opinion { get; set; }


        public Note(string name, string email, string opinion)
        {
            this.Name = name;
            this.Email = email;
            this.Opinion = opinion; 
           
        }

        public string Object_to_json()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}
