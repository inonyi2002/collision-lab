﻿using client.all_classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace client
{
    class Board: Isupport_json
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public double Elasticity { get; set; }
        public Point Reference_location { get; set; }

        public Board(double width, double height, double elasticity, Point reference_location)
        {
            this.Width = width;
            this.Height = height;
            this.Elasticity = elasticity; 
            this.Reference_location = reference_location;
            
        }

        public string Object_to_json()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}
