﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace client.all_classes
{
    [JsonObject(MemberSerialization.OptIn)]
   public class Ball: Isupport_json, INotifyPropertyChanged, ICloneable
    {
        private const double Default_radios = 60;
        private const double Default_mass = 1;

        public Ellipse Graphic_ball { get; set; }
        public int Observable_ball_number { get; set; }
        [JsonProperty]
        public int Ball_number { get; set; }
        [JsonProperty]
        private Point location; //private for the data binding.
        [JsonProperty]
        private double velocity_x { get; set; }
        [JsonProperty]
        private double velocity_y { get; set; }
        [JsonProperty]
        private double mass { get; set; }
        [JsonProperty]
        private double radios { get; set; }
        [JsonProperty]
        private double kinetic_friction { get; set; }
        [JsonProperty]
        private double static_friction { get; set; }

        public Ball(Ellipse Graphic_ball, int Ball_number, Point Location, double Velocity_x,
    double Velocity_y, double Mass, double Radios, double Kinetic_friction,
    double Static_friction)
        {
            this.Graphic_ball = Graphic_ball;
            this.Ball_number = Ball_number;
            this.Location = Location;
            this.Velocity_x = Velocity_x;
            this.Velocity_y = Velocity_y;
            this.Mass = Mass;
            this.Radios = Radios;
            this.Kinetic_friction = Kinetic_friction;
            this.Static_friction = Static_friction;

        }


        public Ball()
        {
            Random rnd = new Random();

            this.Graphic_ball = null;
            this.Ball_number = 0;
            this.Observable_ball_number = 0; 
            this.Location = new Point();
            this.Velocity_x = rnd.Next(450, 650);
            this.Velocity_y = rnd.Next(450, 650);
            this.Mass = Default_mass;
            this.Radios = Default_radios; //rnd.Next(30, 60 + 1); 
            this.Kinetic_friction = 0;
            this.Static_friction = 0;
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        public Point Location
        {
            get { return location; }
            set
            {
                location = new Point(Math.Round(value.X, 3), Math.Round(value.Y, 3));
                OnPropertyChanged("Location");
            }
        }


        public double Velocity_x
        {
            get { return velocity_x; }

            set
            {
                velocity_x = Math.Round(value, 2);
                OnPropertyChanged("Velocity_x");
            }

        }


        public double Velocity_y
        {
            get { return velocity_y; }

            set
            {
                velocity_y = Math.Round(value, 2);
                OnPropertyChanged("Velocity_y");
            }
        }


        public double Mass
        {
            get { return mass; }

            set
            {
                mass = value;
                OnPropertyChanged("Mass");
            }
        }


        public double Radios
        {
            get { return radios; }

            set
            {
                radios = value;
                OnPropertyChanged("Radios");
            }
        }


        public double Kinetic_friction
        {
            get { return kinetic_friction; }

            set
            {
                kinetic_friction = value;
                OnPropertyChanged("Kinetic_friction");
            }
        }

        public double Static_friction
        {
            get { return static_friction; }

            set
            {
                static_friction = value;
                OnPropertyChanged("Static_friction");
            }
        }


        public object Clone()
        {
            return new Ball(Graphic_ball, Ball_number, new Point(location.X, location.Y), velocity_x, 
                velocity_y, Mass, Radios, Kinetic_friction, Static_friction); 
        }


        public string Object_to_json()
        {
            string json = JsonConvert.SerializeObject(this);
            return json; 
        }

    }
}
