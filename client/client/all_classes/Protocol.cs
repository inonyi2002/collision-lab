﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client.all_classes
{
    static class Protocol
    {

        public static readonly string Sep_char = "#"; 





        public static string Get_open_message()
        {
            return "HELLO" + Get_extension(3); 
        }




        public static string Get_all_canvas_data(ObservableCollection<Ball> balls_list, Board cur_board)
        {
            string to_send = "CANVAS_DATA";

            to_send += Sep_char;
            to_send += cur_board.Object_to_json();
            to_send += Sep_char;

            foreach (Ball b in balls_list)
            {
                to_send += b.Object_to_json();
                to_send += Sep_char; 
            }
            
            return to_send + Get_extension(2); 
        }

        public static string Get_note(Note note)
        {
            string to_send = "NOTE";
            to_send += Sep_char;
            to_send += note.Object_to_json();

            return to_send + Get_extension(3); 
        }


        public static string Get_pause()
        {
            return "PAUSE" + Get_extension(); 
        }


        public static string Get_close()
        {
            return "CLOSE" + Get_extension();
        }

        public static string Get_address()
        {
            return "ADDR";
        }


        private static string Get_extension(byte num = 3)
        {
            string str = Sep_char;
            for (byte i = 0; i < num - 1; i++)
                str += Sep_char;
            
            return str; 
        }

    }
}
