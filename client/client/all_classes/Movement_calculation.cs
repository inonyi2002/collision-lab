﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace client.all_classes
{
    static class Movement_calculation
    {
        public const double g = 10;
        public static double delta_time = 0.01; 



        public static double Calc_velocity_x(Point previous_point, Point next_point)
        {
            return round_by_num((next_point.X - previous_point.X) / delta_time); 
        }

        public static double Calc_velocity_y(Point previous_point, Point next_point)
        {
            return round_by_num((next_point.Y - previous_point.Y) / delta_time);
        }

        public static double Calc_vector_velocity(Point previous_point, Point next_point)
        {
            double velocity_x = Calc_velocity_x(previous_point, next_point);
            double velocity_y = Calc_velocity_y(previous_point, next_point);
            double vector_velocity = Math.Sqrt(Math.Pow(velocity_x, 2) + Math.Pow(velocity_y, 2));
            return round_by_num(vector_velocity); 
        }

        public static double Calc_angle(Point previous_point, Point next_point)
        {
            double delta_y = next_point.Y - previous_point.Y;
            double delta_x = next_point.X - previous_point.X;
            double angle = (Math.Atan(delta_y / delta_x) * 180) / Math.PI;
            return round_by_num(angle); 
        }

        public static double Calc_acceleration(Ball b)
        {
            double acc = b.Kinetic_friction * g * -1;
            return round_by_num(acc, 2); 
        }

        public static double Calc_total_energy(ObservableCollection<Ball> balls_list)
        {
            double total_energy = 0;
       
            foreach  (Ball b in balls_list)
            {
                double v = Math.Pow(b.Velocity_x, 2) + Math.Pow(b.Velocity_y, 2);
                total_energy += (b.Mass * v * 0.5); 
            }
            return round_by_num(total_energy, 2); 
        }


        public static double Distance(Point p1, Point p2)
        {
            double delta_x = Math.Abs(p1.X - p2.X); 
            double delta_y = Math.Abs(p1.Y - p2.Y);

            return Math.Sqrt(Math.Pow(delta_x, 2) + Math.Pow(delta_y, 2)); 
        }


/// private functions 


        private static double round_by_num(double num, int digit_after_dot = 3)
        {
            return Math.Round(num, digit_after_dot);
        }


    }
}
