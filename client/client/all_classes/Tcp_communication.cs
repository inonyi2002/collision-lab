﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace client.all_classes
{
    static class Tcp_communication 
    {
        private static Socket Tcp_socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        private const int Port = 2002;
        private static readonly string Ip = "127.0.0.1";

        private static readonly string SIZE_HEADER_FORMAT = "000000000|";
        private static readonly int size_header_size = SIZE_HEADER_FORMAT.Length; 

        public static bool Connect_to_server()
        {
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Ip), Port);

            try
            {
                Tcp_socket.Connect(ep);
            }
            catch (Exception  ex)
            {

                return false; 
            }

            return true; 
        }

        public static void Send(string data)
        {
            

            int len_data = data.Length;
            string header_data = len_data.ToString().PadLeft(size_header_size - 1, '0') + "|"; 
            byte[] msg = Encoding.ASCII.GetBytes(header_data + data);

            Tcp_socket.Send(msg); 
        }


        public static string Receive()
        {

            string size_header = "";
            int data_len = 0;

            while (size_header.Length < size_header_size)
            {

                byte[] header_buffer = new byte[size_header_size - size_header.Length];
                Tcp_socket.Receive(header_buffer);
                string _s = Encoding.UTF8.GetString(header_buffer);

                if (_s == "")
                    break; 

                else
                    size_header += _s;
            }

            string data = ""; 
            if(size_header != "")
            {
                data_len = int.Parse(size_header.Substring(0, size_header_size - 1));
                while (data.Length < data_len)
                {
                    byte[] data_buffer = new byte[data_len - data.Length];
                    Tcp_socket.Receive(data_buffer);
                    string _d = Encoding.UTF8.GetString(data_buffer);

                    if (_d == "")
                        break;

                    else
                        data += _d;
                }
            }

            return data;
        }


        public static void End_connection()
        {
            Tcp_socket.Close(); 
        }

    }
}
