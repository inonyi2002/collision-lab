﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace client.all_classes
{
    class Point_json
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point_json(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point Convert_to_point()
        {
            return new Point(this.X, this.Y);
        }

        public override string ToString()
        {
            return "(" + this.X.ToString() + "," + this.Y.ToString() + ")";
        }
    }
}
