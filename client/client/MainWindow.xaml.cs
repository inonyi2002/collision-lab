﻿using client.all_classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
        }

        private void Window_loaded(object sender, RoutedEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\records";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            bool is_connect = Tcp_communication.Connect_to_server();

            if (!is_connect)
                MainFrame.NavigationService.Navigate(new Connection_failed_page());

            else
            {
                Tcp_communication.Send("HELLO###");
                string data = Tcp_communication.Receive();

                if (data == "OLLHE###")
                {
                    MainFrame.NavigationService.Navigate(new Open_page());
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }

        }
    }
}
